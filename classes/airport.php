<?php

require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";
class Airport extends DB {
    protected $data=array(
        "id" => 0,
        "name" => "",
        "city_id" => 0,
        "city_name" => "",
        "address" => ""
    );

    public static function getAllAirports(){
        $connection = self::connect();
        $query = "select airports.*,cities.name as 'city_name' from airports,cities where airports.city_id = cities.id";
        $result = $connection->query($query);
        self::discoonect($connection);
        if($result->num_rows){
            $airports = array();
            foreach ($result->fetch_all(MYSQLI_ASSOC) as $row){
                $airports[] = new Airport($row);
            }
            return $airports;
        }
        else{
            return null;
        }
    }
}