<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";

class Bookmark extends DB{
    protected $data = array(
        "id" => 0,
        "user_id" => 0,
        "flight_plan_id" => 0
    );

    public static function getAllBookmarksByUserId($user_id){
        $connection = self::connect();
        $result = $connection->query("select `flight_plan_id` from bookmarks where `user_id`='$user_id'");
        if($result->num_rows){
            $bookmarked = array();
            $arr = $result->fetch_all();
            foreach ($arr as $x){
                $bookmarked[] = $x[0];
            }
            self::discoonect($connection);
            return $bookmarked;
        }
    }

    public static function addBookmark($user_id,$flight_plan_id){
        if(self::isBookmark($user_id,$flight_plan_id)){
            return -1;
        }
        else{
            $connection = self::connect();
            $query = "insert into bookmarks(`user_id`,`flight_plan_id`) values ($user_id,$flight_plan_id)";
            $result = $connection->query($query);
            if($result) {
                self::discoonect($connection);
                return 1;
            }
            else {
                self::discoonect($connection);
                return -2;
            }
        }
    }

    public static function removeBookmark($user_id,$flight_plan_id){
        if(!self::isBookmark($user_id,$flight_plan_id)){
            return -1;
        }
        else{
            $connection = self::connect();
            $query = "delete from bookmarks where `user_id`=$user_id and `flight_plan_id`=$flight_plan_id";
            $result = $connection->query($query);
            if($result) {
                self::discoonect($connection);
                return 1;
            }
            else {
                self::discoonect($connection);
                return -2;
            }
        }
    }

    public static function isBookmark($user_id,$flight_plan_id){
        $connection = self::connect();
        $query = "select * from bookmarks where `user_id`=$user_id and `flight_plan_id`=$flight_plan_id";
        $result = $connection->query($query);
        if($result->num_rows) {
            self::discoonect($connection);
            return true;
        }
        else {
            self::discoonect($connection);
            return false;
        }
    }

    public static function isBookmarkExist($user_id){
        $connection = self::connect();
        $query = "select * from bookmarks where `user_id`=$user_id";
        $result = $connection->query($query);
        if($result->num_rows){
            self::discoonect($connection);
            return true;
        }
        else{
            self::discoonect($connection);
            return false;
        }
    }
}