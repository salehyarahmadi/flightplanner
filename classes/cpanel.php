<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";

class Cpanel{
    public static $SUPER_ADMIN_MENU = 1;
    public static $ADMIN_MENU = 2;
    public static $USER_MENU = 3;

    public static function generateBookmarkedPage(){
        if($_SESSION['user_type_id']!=self::$USER_MENU){
            redirect("index.php");
        }
?>
        <h1 style="color:black;padding:100px 0 10px 40px;">Bookmarked Flights</h1>

        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for ..." title="Type in an airline name">
        <select id="mySelect">
            <option value="flight_number">Flight Number</option>
            <option value="airline">AirLine</option>
            <option value="origin">Origin</option>
            <option value="destination">Destination</option>
        </select>
        <div style="margin-right: 10px;margin-left: 10px;">
        <table id="myTable">
            <tr class="header">
                <th style="width:10%;">Fight Number</th>
                <th style="width:11%;">Airline</th>
                <th style="width:15%;">Origin</th>
                <th style="width:15%;">Destination</th>
                <th style="width:15%;">TakeOff Time</th>
                <th style="width:15%;">Landing Delay</th>
                <th style="width:15%;">Landing Time</th>
                <th style="width: 4%">Bookmark</th>
            </tr>
            <?php
            $bookmarked = Bookmark::getAllBookmarksByUserId($_SESSION['user_id']);
            $bookmarked_flight_plans = FlightPlan::getAllFlightPlanBySomeFlightPlanId($bookmarked);
            if(is_array($bookmarked_flight_plans)){
            foreach ($bookmarked_flight_plans as $flight_plan){
                ?>
                    <tr>
                        <td><?php echo $flight_plan->flight_number; ?></td>
                        <td><?php echo $flight_plan->agency_name; ?></td>
                        <td><span style="font-size: small;"><?php echo $flight_plan->source_airport_name; ?></span><br><?php echo FlightPlan::getCityNameByAirportId($flight_plan->source_airport_id); ?></td>
                        <td><span style="font-size: small;"><?php echo $flight_plan->destination_airport_name; ?></span><br><?php echo FlightPlan::getCityNameByAirportId($flight_plan->destination_airport_id); ?></td>
                        <td><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay); ?></td>
                        <td><?php echo ($flight_plan->end_delay / 60)." minutes"; ?></td>
                        <td style="font-weight: bold;"><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay + $flight_plan->duration + $flight_plan->end_delay); ?></td>
                        <?php $id="mystar".$flight_plan->id; ?>
                        <?php
                        $color = (Bookmark::isBookmark($_SESSION['user_id'],$flight_plan->id)) ? 'yellow' : 'gray';
                        ?>
                        <td><i onclick="lighter('<?php echo $id; ?>','<?php echo $flight_plan->id; ?>','<?php echo $_SESSION['user_id'] ?>');" id="<?php echo $id; ?>" class="far fa-star" style="color: <?php echo $color; ?>;" ></i></td>
                    </tr>
                <?php
                }}
            ?>
        </table>
<?php
    }

    public static function generateAllFlightsPage(){
        if($_SESSION['user_type_id']!=self::$USER_MENU){
            redirect("index.php");
        }
        ?>
        <h1 style="color:black;padding:100px 0px 10px 40px;">All Flights</h1>

        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for ..." title="Type in an airline name">
        <select id="mySelect">
            <option value="flight_number">Flight Number</option>
            <option value="airline">AirLine</option>
            <option value="origin">Origin</option>
            <option value="destination">Destination</option>
        </select>
        <div style="margin-right: 10px;margin-left: 10px;">
        <table id="myTable">
            <tr class="header">
                <th style="width:10%;">Fight Number</th>
                <th style="width:11%;">Airline</th>
                <th style="width:15%;">Origin</th>
                <th style="width:15%;">Destination</th>
                <th style="width:15%;">TakeOff Time</th>
                <th style="width:15%;">Landing Delay</th>
                <th style="width:15%;">Landing Time</th>
                <th style="width: 4%">Bookmark</th>
            </tr>
            <?php
            $flight_plans = FlightPlan::getAllFlightPlans();
            if(is_array($flight_plans)){
                foreach ($flight_plans as $flight_plan){
                    ?>
                    <tr>
                        <td><?php echo $flight_plan->flight_number; ?></td>
                        <td><?php echo $flight_plan->agency_name; ?></td>
                        <td><span style="font-size: small;"><?php echo $flight_plan->source_airport_name; ?></span><br><?php echo FlightPlan::getCityNameByAirportId($flight_plan->source_airport_id); ?></td>
                        <td><span style="font-size: small;"><?php echo $flight_plan->destination_airport_name; ?></span><br><?php echo FlightPlan::getCityNameByAirportId($flight_plan->destination_airport_id); ?></td>
                        <td><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay); ?></td>
                        <td><?php echo ($flight_plan->end_delay / 60)." minutes"; ?></td>
                        <td style="font-weight: bold;"><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay + $flight_plan->duration + $flight_plan->end_delay); ?></td>
                        <?php $id="mystar".$flight_plan->id; ?>
                        <?php
                        $color = (Bookmark::isBookmark($_SESSION['user_id'],$flight_plan->id)) ? 'yellow' : 'gray';
                        ?>
                        <td><i onclick="lighter('<?php echo $id; ?>','<?php echo $flight_plan->id; ?>','<?php echo $_SESSION['user_id'] ?>');" id="<?php echo $id; ?>" class="far fa-star" style="color: <?php echo $color; ?>;" ></i></td>
                    </tr>
                    <?php
                }}
            ?>
        </table>
        <?php
    }

    public static function generateChangePasswordPage(){
?>
       <div style="text-align:center;padding:90px 0px 50px 0px;margin:20px;">
        <br />
        <div class="alert alert-danger error-alert">
            <strong>Error!</strong>
            <span id="error-details">Old password is incorrect!</span>
        </div>
        <form method="post" action="../response.php" name="changepassword-form" onsubmit="return check('<?php echo $_SESSION['user_id']; ?>')">
            <input type="text" class="myInput" name="oldpassword" id="oldpassword" placeholder="Enter the old password ..." title="enter old pass">
            <input type="text" class="myInput" name="newpassword" id="newpassword" placeholder="Enter the new password ..." title="enter new pass">
            <input type="text" class="myInput" name="c_newpassword" id="c_newpassword" placeholder="Enter the new password again ..." title="enter new pass again">
            <input type="submit" class="myInput" name="changepassword_submit" id="change_password_submit" value="Chage Password" />
        </form>
        <br />
        <a href="#" class="wow fadeInUp" data-wow-delay="0.3s" style="display:block; margin-top:50px;font-size:15px;color:#111112;text-decoration:underline;">Home Page</a>
    </div>
<?php
    }

    public static function generateOnDeskFlightsForAdminPage(){
        if($_SESSION['user_type_id']!=self::$ADMIN_MENU){
            redirect("index.php");
        }
        ?>
        <h1 style="color:black;padding:100px 0px 10px 40px;">All Flights</h1>


        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for ..." title="Type in an airline name">
        <select id="mySelect">
            <option value="flight_number">Flight Number</option>
            <option value="airline">AirLine</option>
            <option value="origin">Origin</option>
            <option value="destination">Destination</option>
        </select>
        <div style="margin-right: 10px;margin-left: 10px;">
        <a href="http://localhost/FlightPlanner/cpanel?page=addflight" style="text-align:center;margin:10px 0 10px 0;display:block;font-size: 20px;font-weight: 400;padding: 10px 20px 10px 20px; background-color: #ddd;width: 100%;">Add</a>
        <table id="myTable">
            <tr class="header">
                <th style="width:10%;">Fight Number</th>
                <th style="width:11%;">Airline</th>
                <th style="width:15%;">Origin</th>
                <th style="width:15%;">Destination</th>
                <th style="width:15%;">TakeOff Time</th>
                <th style="width:15%;">Landing Delay</th>
                <th style="width:15%;">Landing Time</th>
                <th style="width: 4%">Actions</th>
            </tr>
            <?php
            $flight_plans = FlightPlan::getFlightsByAdminId($_SESSION['user_id']);
            if(is_array($flight_plans)){
                foreach ($flight_plans as $flight_plan){
                    $row_id = "row".$flight_plan->id;
                    ?>
                    <tr id="<?php echo $row_id; ?>">
                        <td><?php echo $flight_plan->flight_number; ?></td>
                        <td><?php echo $flight_plan->agency_name; ?></td>
                        <td><span style="font-size: small;"><?php echo $flight_plan->source_airport_name; ?></span><br><?php echo FlightPlan::getCityNameByAirportId($flight_plan->source_airport_id); ?></td>
                        <td><span style="font-size: small;"><?php echo $flight_plan->destination_airport_name; ?></span><br><?php echo FlightPlan::getCityNameByAirportId($flight_plan->destination_airport_id); ?></td>
                        <td><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay); ?></td>
                        <td><?php echo ($flight_plan->end_delay / 60)." minutes"; ?></td>
                        <td style="font-weight: bold;"><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay + $flight_plan->duration + $flight_plan->end_delay); ?></td>
                        <td><a id="btn-edit" href="http://localhost/FlightPlanner/cpanel/index.php?page=editflight&flight_plan_id=<?php echo $flight_plan->id; ?>" role="button">Edit</a><a id="btn-delete" href="#" onclick="deleterow('<?php echo $row_id; ?>',<?php echo $flight_plan->id; ?>)" role="button">Delete</a></td>
                    </tr>
                    <?php
                }}
            ?>
        </table>
        <?php
    }
    
    public static function generateAddFlightPage(){
        if($_SESSION['user_type_id']!=Cpanel::$ADMIN_MENU){
            redirect("index.php");
        }
?>
    <div style="text-align:center;padding:90px 0px 50px 0px;margin:20px;">
        <br />
        <form method="post" action="../response.php">

            <div class="row" style="margin-bottom: 10px;">
                <div class ="col-sm-6" >
                    <input name="flight_number"  type="text" id="sel1" placeholder="Enter the flight number..." title="enter flight number" style="width:70%;" />
                </div>
                <?php $airports = Airport::getAllAirports(); ?>
                <div class ="col-sm-6">
                    <select name="airport" class="form-control" id="sel1" style="width:74%;margin:auto;">
                        <?php
                           foreach ($airports as $airport){
                               ?>
                               <option value="<?php echo $airport->id; ?>"><?php echo $airport->city_name.', '.$airport->name; ?></option>
                               <?php
                           }
                         ?>
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input name="duration" type="text" id="sel2" placeholder="Enter the duration of the flight (hour)..." title="enter duration" style="width:70%;" />
                </div>
                <div id="datetimepicker" class="col-sm-6 input-append date" >
                    <input name="takeoff_time" type="text" placeholder="Pick the date..." style="height:50px;border-radius:0;background-color:#f1f1f1;width:70%;" />
                    <span class="add-on" style="height:50px;border-radius:0;">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" style="font-size:0;border-radius:0;"></i>
                    </span>
                </div>

            </div>
            <input type="submit" class="myInput" name="add-submit" id="addflightsubmit" value="Add flight" />

        </form>
        <br />
        <a href="#" class="wow fadeInUp" data-wow-delay="0.3s" style="display:block; padding-top:50px;font-size:15px;color:#f1f1f1;text-decoration:underline;">Home Page</a>
    </div>
<?php
    }

    public static function generateEditFlightPage($is_origin,$flight_plan_id){
        if($_SESSION['user_type_id']!=Cpanel::$ADMIN_MENU){
            redirect("index.php");
        }
        if($is_origin){
            $placeholder = "Enter Takeoff Delay(hour)...";
            $name = "takeoff_delay";
        }
        else{
            $placeholder = "Enter Landing Delay(hour)...";
            $name = "landing_delay";
        }
?>
    <div style="text-align:center;padding:90px 0px 50px 0px;margin:20px;">
        <br />
        <?php $flight_plan = FlightPlan::getFlightPlanByFlightPlanID($flight_plan_id); ?>
        <form method="post" action="../response.php">
            <div class="row" style="margin-bottom: 10px;">
                <?php if($is_origin){ ?>
                <div class ="col-sm-6" >
                    <input name="edit_flight_number"  type="text" id="sel1" placeholder="Current flight number is <?php echo $flight_plan->flight_number; ?>" title="enter flight number" style="width:70%;" />
                </div>
                <?php } ?>
                <?php $airports = Airport::getAllAirports(); ?>
                <div class ="col-sm-6">
                    <select name="edit_airport" class="form-control" id="sel1" style="width:74%;margin:auto;">
                        <?php
                           foreach ($airports as $airport){
                               ?>
                               <option value="<?php echo $airport->id; ?>"><?php echo $airport->city_name.', '.$airport->name; ?></option>
                               <?php
                           }
                         ?>
                    </select>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input name="<?php echo $name; ?>" type="text" id="sel2" placeholder="<?php echo $placeholder; ?>" title="enter duration" style="width:70%;" />
                </div>
                <?php if($is_origin){ ?>
                <div id="datetimepicker" class="col-sm-6 input-append date" >
                    <input name="edit_takeoff_time" type="text" placeholder="Pick the date..." style="height:50px;border-radius:0;background-color:#f1f1f1;width:70%;" />
                    <span class="add-on" style="height:50px;border-radius:0;">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" style="font-size:0;border-radius:0;"></i>
                    </span>
                </div>
                <?php } ?>

            </div>
            <input type="submit" class="myInput" name="edit-submit" id="editflightsubmit" value="Update flight" />
            <input type="hidden" name="flight_plan_id" value="<?php echo $flight_plan_id; ?>">
        </form>
        <br />
        <a href="#" class="wow fadeInUp" data-wow-delay="0.3s" style="display:block; padding-top:50px;font-size:15px;color:#f1f1f1;text-decoration:underline;">Home Page</a>
    </div>
<?php
    }

    public static function generateMenu($menu){
        if($menu == self::$SUPER_ADMIN_MENU){

        }
        elseif ($menu == self::$ADMIN_MENU){
            ?>
            <li><a href="http://localhost/FlightPlanner">Home</a></li>
            <li><a href="http://localhost/FlightPlanner/cpanel?page=mydesk">My Desk</a></li>
            <li><a href="http://localhost/FlightPlanner/cpanel?page=changepassword">Change Password</a></li>
            <li><a href="http://localhost/FlightPlanner/response.php?action=logout">Log Out</a></li>
            <?php
        }
        elseif ($menu == self::$USER_MENU){
            ?>
            <li><a href="http://localhost/FlightPlanner">Home</a></li>
            <li><a href="http://localhost/FlightPlanner/cpanel?page=bookmarked">Bookmarked Flights</a></li>
            <li><a href="http://localhost/FlightPlanner/cpanel?page=flights">All Flights</a></li>
            <li><a href="http://localhost/FlightPlanner/cpanel?page=changepassword">Change Password</a></li>
            <li><a href="http://localhost/FlightPlanner/response.php?action=logout">Log Out</a></li>
            <?php
        }
        else{

        }
    }
}