<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";

class Date{
    public static function formatter($time){
        $arr = convertDate($time);
        $string = "<span style='font-size: small;'>".$arr['weekday_name'].', '.$arr['month_name'].' '.$arr['day'].', '.$arr['year']."</span><br>".$arr['hour'].':'.$arr['minute'];
        return $string;
    }
}