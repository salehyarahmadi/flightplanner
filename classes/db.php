<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";

class DB{
    protected $data = array();
    public function __construct($data){
        if(is_array($data) && count($data) > 0) {
            foreach ($data as $key => $value) {
                if (array_key_exists($key, $this->data))
                    $this->data[$key] = $value;
            }
        }
    }

    public function __get($property){
        if(array_key_exists($property , $this->data))
            return $this->data[$property];
        else
            die("Invalid Property");
    }

    protected static function connect(){
        $connection = new mysqli(DB_HOST,DB_USERNAME,DB_PASSWORD,DB_NAME);
        $connection->set_charset("utf8");
        return $connection;
    }

    protected static function discoonect($connection){
        unset($connection);
    }
}