<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";

class FlightPlan extends DB{
    protected $data=array(
        "id" => 0,
        "flight_number" => "",
        "source_airport_id" => 0,
        "destination_airport_id" => 0,
        "agency_id" => 0,
        "start_time" => 0,
        "start_delay" => 0,
        "duration" => 0,
        "end_delay" => 0,
        "agency_name" => "",
        "source_airport_name" => "",
        "destination_airport_name" => ""
    );

    public static function getAllFlightPlans(){
        $connection = DB::connect();
        $time = time() - 3600;
        $query = "select distinct flight_plan.*, agencies.name as 'agency_name', s.name as 'source_airport_name', d.name as 'destination_airport_name' from flight_plan,agencies,airports as s , airports as d where flight_plan.agency_id = agencies.id and flight_plan.source_airport_id = s.id and flight_plan.destination_airport_id = d.id and start_time+start_delay+duration+end_delay > $time ";
        $result = $connection->query($query);
        if($result->num_rows){
            $flight_plans = array();
            foreach ($result->fetch_all(MYSQLI_ASSOC) as $row){
                $flight_plans[] = new FlightPlan($row);
            }
            DB::discoonect($connection);
            return $flight_plans;
        }
        else{
            DB::discoonect($connection);
            return null;
        }
    }

    public static function getFlightPlanByFlightPlanID($flight_plan_id){
        $connection = DB::connect();
        $query = "select distinct flight_plan.*, agencies.name as 'agency_name', s.name as 'source_airport_name', d.name as 'destination_airport_name' from flight_plan,agencies,airports as s , airports as d where flight_plan.agency_id = agencies.id and flight_plan.source_airport_id = s.id and flight_plan.destination_airport_id = d.id and flight_plan.id = $flight_plan_id";
        $result = $connection->query($query);
        $flight_plan = null;
        if($result->num_rows){
            $flight_plan = new FlightPlan($result->fetch_all(MYSQLI_ASSOC)[0]);
            DB::discoonect($connection);
            return $flight_plan;
        }
        else{
            DB::discoonect($connection);
            return null;
        }
    }

    public static function getAllFlightPlanBySomeFlightPlanId($bookmarked){
        if(is_array($bookmarked)){
            $condition = "";
            foreach ($bookmarked as $id){
                $condition.="flight_plan.id=";
                $condition.=$id;
                $condition.=" or ";
            }
            $condition.="false";
            $connection = self::connect();
            $query = "select distinct flight_plan.*, agencies.name as \"agency_name\", s.name as \"source_airport_name\", d.name as \"destination_airport_name\" from flight_plan,agencies,airports as s , airports as d where flight_plan.agency_id = agencies.id and flight_plan.source_airport_id = s.id and flight_plan.destination_airport_id = d.id and ($condition)";
            $result = $connection->query($query);
            //$result = $connection->query("select * from flight_plan where $condition");
            if($result->num_rows){
                $flight_plans = array();
                foreach ($result->fetch_all(MYSQLI_ASSOC) as $row){
                    $flight_plans[] = new FlightPlan($row);
                }
                DB::discoonect($connection);
                return $flight_plans;
            }
            else{
                self::discoonect($connection);
                return false;
            }
        }
    }

    public static function getCityNameByAirportId($airport_id){
        $connection = self::connect();
        $query = "select cities.name from airports,cities where airports.city_id = cities.id and airports.id = $airport_id";
        $result = $connection->query($query);
        self::discoonect($connection);
        if($result->num_rows)
            return $result->fetch_all(MYSQLI_ASSOC)[0]['name'];
        else
            return false;
    }

    public static function getFlightsByAdminId($admin_id){
        $connection = DB::connect();
        $time = time() - 3600;
        $query = "select f.*, agencies.name as 'agency_name', a1.name as 'source_airport_name', a2.name as 'destination_airport_name'  from flight_plan as f, airport_agency as aa, airports as a1, airports as a2, agencies where f.agency_id = aa.agency_id and aa.agency_admin_id = $admin_id and  (f.destination_airport_id = aa.airport_id or f.source_airport_id = aa.airport_id) and a1.id = f.source_airport_id and a2.id = f.destination_airport_id and f.agency_id = agencies.id and start_time+start_delay+duration+end_delay > $time";
        $result = $connection->query($query);
        if($result->num_rows){
            $flight_plans = array();
            foreach ($result->fetch_all(MYSQLI_ASSOC) as $row){
                $flight_plans[] = new FlightPlan($row);
            }
            DB::discoonect($connection);
            return $flight_plans;
        }
        else{
            DB::discoonect($connection);
            return null;
        }
    }

    public static function deleteFlightPlanById($flight_plan_id){
        $connection = self::connect();
        $query = "delete from flight_plan where `id`=$flight_plan_id";
        $result = $connection->query($query);
        self::discoonect($connection);
        if($result)
            return true;
        else
            return false;
    }

    public static function getAirportIdAndAgencyIdByAdminId(){
        $admin_id = $_SESSION['user_id'];
        $connection = self::connect();
        $query = "select * from airport_agency where agency_admin_id = $admin_id";
        $result = $connection->query($query);
        $x = array();
        if($result->num_rows){
            $tmp = $result->fetch_all(MYSQLI_ASSOC)[0];
            $airport_id =  $tmp['airport_id'];
            $agency_id =  $tmp['agency_id'];
            $x[]=$airport_id;
            $x[]=$agency_id;
            return $x;
        }
        else{
            return null;
        }
    }

    public static function addFlightPlan($flight_number,$destination_airport_id,$duration,$takeoff_time){
        $connection = self::connect();
        $x = self::getAirportIdAndAgencyIdByAdminId();
        $query = "insert into flight_plan(flight_number,source_airport_id,destination_airport_id,agency_id,start_time,start_delay,duration,end_delay) values('$flight_number',$x[0],$destination_airport_id,$x[1],$takeoff_time,0,$duration,0)";
        $result = $connection->query($query);
        if($result){
            return true;
        }
        else
            return false;
    }

    public static function isInOrigin($flight_plan_id){
        $x = self::getAirportIdAndAgencyIdByAdminId();
        $connection = self::connect();
        $query = "select * from flight_plan where `agency_id`=$x[1] and `source_airport_id`=$x[0] and `id`=$flight_plan_id";
        $result = $connection->query($query);
        self::discoonect($connection);
        if($result->num_rows){
            return true;
        }
        else{
            return false;
        }
    }

    public static function update($flight_number,$destination_airport_id,$takeoff_time,$delay, $is_origin, $flight_plan_id){
        $connection = self::connect();
        $delay *= 3600;
        if($is_origin)
            $query = "update flight_plan set flight_number=$flight_number, destination_airport_id=$destination_airport_id, start_time=$takeoff_time, start_delay=$delay where `id`=$flight_plan_id";
        else
            $query = "update flight_plan set start_time=$takeoff_time, end_delay=$delay where `id`=$flight_plan_id";
        echo $query;
        $result = $connection->query($query);
        if($result){
            return true;
        }
        else{
            return false;
        }
    }
}