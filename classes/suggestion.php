<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";

class Suggestion extends DB{
    protected $data = array(
        "id" => 0,
        "name" => "",
        "email" => "",
        "message" => ""
    );

    public static function addSuggestion($name,$email,$message){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) or strlen($name) < 3) {
            return false;
        }
        $connection = self::connect();
        $result = $connection->query("insert into suggestions(`name`,`email`,`message`) VALUES ('$name','$email','$message')");
        if($result){
            self::discoonect($connection);
            return true;
        }
        else{
            self::discoonect($connection);
            return false;
        }
    }
}