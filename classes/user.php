<?php
require_once __DIR__.DIRECTORY_SEPARATOR."../include.php";
class User extends DB{
    protected $data=array(
        "id" => 0,
        "first_name" => "",
        "last_name" => "",
        "email" => "",
        "password" => 0,
        "is_active" => false,
        "activation_code" => "",
        "user_type_id" => 0
    );

    public static function getAllUsers(){
        $connection = self::connect();
        $result = $connection->query("SELECT * FROM users");
        if($result->num_rows){
            $users = array();
            foreach ($result->fetch_all(MYSQLI_ASSOC) as $row){
                $users[] = new User($row);
            }
            DB::discoonect($connection);
            return $users;
        }
        else{
            DB::discoonect($connection);
            return null;
        }

    }

    public static function addUserToUsers($email,$password,$confirm_password){
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return -1;
        }

        $connection = self::connect();
        $result = $connection->query("select email from users where `email`='$email'");
        if($result->num_rows){
            self::discoonect($connection);
            return -2;
        }

        if($password != $confirm_password){
            return -3;
        }

        if (strlen($password) < 8) {
            return -4;
        }

        if (!preg_match("#[0-9]+#", $password)) {
            return -5;
        }

        if (!preg_match("#[a-zA-Z]+#", $password)) {
            return -6;
        }

        $hashed_password = md5($password);
        $activation_code = rand(10000000,99999999);
        $result = $connection->query("insert into users (email,password,is_active,activation_code,user_type_id) values ('$email','$hashed_password',false,'$activation_code',3);");
        if($result) {
            self::discoonect($connection);
            if(self::sendActivationEmail($email,$activation_code))
                return 1;
            else
                return -7;
        }
        else {
            self::discoonect($connection);
            return 0;
        }
    }

    public static function sendActivationEmail($email,$activation_code){
        $mail = new \PHPMailer\PHPMailer\PHPMailer();
        $mail->IsSMTP();
        $mail->SMTPAuth   = true;
        //$mail->SMTPDebug = 2;
        $mail->Debugoutput = 'html';
        $mail->isHTML(true);
        $mail->Host       = "smtp.gmail.com";
        $mail->Username   = "salehyarahmadi96@gmail.com";
        $mail->Password   = "s1281996s";
        $mail->SMTPSecure = 'ssl';
        $mail->Port       = 465; //465 587

        $mail->setFrom('salehyarahmadi96@gmail.com', 'Pine Team');
        $mail->addAddress($email, "Pine Team");

        $mail->Subject    = "Activation Code";
        /*$mail->Body = <<<EOS
<p style="direction: rtl; font-family: Tahoma;">
To activate your account and verify your email address, please click the following link:<br />
<a href="http://localhost/FlightPlanner/response.php?action=activate&code=$activation_code" target="_blank">
http://localhost/FlightPlanner/?action=activate&amp;code=$activation_code
</a>
</p>
EOS;*/

        $mail->Debugoutput = "html";
        $mail->Body = <<<EOS
<!DOCTYPE html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <title></title>


    <link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


    <style>
        body
	{
		background-color:#dedede;
        font-family: 'Rajdhani', sans-serif;
        font-weight: 400;
	}
    </style>
</head>
<body>
    <div style=" background:#f1c11a;font-size:40px;color:#f1f1f1;text-align:center;padding:20px;font-weight:500;">
        <span style="font-size:smaller;">Welcome to </span>
        <span style="color:#4b4b4b;">Flight Planner</span>
    </div>
    <div>
        <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="50" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" style="margin-top:-1px;">
            <path style="fill: #f1c11a;" d="M0 0 L50 100 L100 0 Z"></path>
        </svg>
    </div>
    <p style="fill: #f1c11a;text-align:center;font-size:25px;padding:40px;">Confirm your email   address to get started with <span style="color:#858585">Flight Planner</span> </p>
    <div style="text-align:center;">
        <a class="btn btn-dark" href="http://localhost/FlightPlanner/response.php?action=activate&code=$activation_code" target="_blank" role="button" style="position:center; color:#f1c11a;font-size:20px;">Confirm My Email</a>
    </div>


    <div style="text-align:center;padding-top:70px;">
        <hr style="width:50%" />
        <h2 style="color:#f1c11a;padding-top:20px;">Responsive</h2>
        <p style="padding:0% 30% 0% 30%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus lacus nulla, eget varius justo tristique ut.</p>
    </div>
    <div style="text-align:center;padding-top:30px;">
        <hr style="width:50%" />
        <h2 style="color:#f1c11a;padding-top:20px;">Up To Date</h2>
        <p style="padding:0% 30% 0% 30%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus lacus nulla, eget varius justo tristique ut.</p>
    </div>
    <div style="text-align:center;padding-top:30px;">
        <hr style="width:50%" />
        <h2 style="color:#f1c11a;padding-top:20px;">Support</h2>
        <p style="padding:0% 30% 50px 30%;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus lacus nulla, eget varius justo tristique ut.</p>
    </div>

    <footer style="background-color:#f1c11a;padding-bottom:10px;">
        <svg class="svgcolor-light" preserveAspectRatio="none" viewBox="0 0 100 102" height="50" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" style="margin-top:-1px;">
            <path style="fill:#dedede;" d="M0 0 L50 100 L100 0 Z"></path>
        </svg>
        <p style="font-size:20px;text-align:center;">DONT MAKE ME WALK WHEN I CAN <span style="color:white;">FLY</span></p>
    </footer>

</body>
</html>
EOS;



        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        return $mail->send();
    }

    public static function Login($email,$password){
        $connection = self::connect();
        $hashed_password = md5($password);
        $result = $connection->query("select * from users where `email`='$email' and `password`='$hashed_password' and is_active=1");
        if($result->num_rows) {
            self::discoonect($connection);
            return new User($result->fetch_assoc());
        }
        else {
            self::discoonect($connection);
            return false;
        }

    }

    public static function Logout(){
        session_destroy();
        header("Location: ./");
    }

    public static function activeUser($activation_code){
        $connection = self::connect();
        $result = $connection->query("update users set `is_active`=1 where `activation_code`='$activation_code'");
        if($result){
            self::discoonect($connection);
            return true;
        }
        else{
            self::discoonect($connection);
            return false;
        }
    }

    public static function changePassword($user_id,$old_password,$new_password){
        $connection = self::connect();
        $hashed_old_password = md5($old_password);
        $query = "select * from users where `id`=$user_id and `password`='$hashed_old_password'";
        $result = $connection->query($query);
        if($result->num_rows){
            if (strlen($new_password) < 8) {
                return -1;
            }
            else if (!preg_match("#[0-9]+#", $new_password)) {
                return -2;
            }
            else if (!preg_match("#[a-zA-Z]+#", $new_password)) {
                return -3;
            }
            else{
                $password = md5($new_password);
                $query = "update users set `password`='$password' where `id`=$user_id";
                $result = $connection->query($query);
                if($result)
                    return 1;
                else
                    return -4;
            }
        }
        else{
            return 0;
        }
    }

    public static function checkUserPassword($user_id,$password){
        $connection = self::connect();
        $hashed_password = md5($password);
        $query = "select * from users where `id`=$user_id and `password`='$hashed_password'";
        $result = $connection->query($query);
        self::discoonect($connection);
        if($result->num_rows)
            return true;
        else
            return false;
    }
}