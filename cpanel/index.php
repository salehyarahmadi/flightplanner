<?php if (!isset($_SESSION)) { session_start(); } ?>
<?php require_once __DIR__.DIRECTORY_SEPARATOR."../include.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pure Drawer</title>
    <?php
    if((isset($_GET['page']) && $_GET['page']=="addflight") or (isset($_GET['page']) && $_GET['page']=="editflight") ){
    ?>
    <link href="http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" media="screen"
          href="http://tarruda.github.com/bootstrap-datetimepicker/assets/css/bootstrap-datetimepicker.min.css">
    <?php } ?>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/pure-drawer.css" />





    <!-- stylesheets css -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.min.css">

    <link rel="stylesheet" href="../css/et-line-font.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="css/vegas.min.css">-->
    <link rel="stylesheet" href="../css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>



    <style>
        #myMenu{
            list-style-type: none;
            margin-top: 150px;


        }
        #myMenu li{
            height:100%;
            padding-bottom: 20px;
        }

        #myMenu a{
            height:100%;
            padding:10px 15px;
            text-decoration: none;
            font-size:1.5rem;
            color: gray;
            transition: 0.2s;


        }
        #myMenu a:hover{
            color: #111112;
            transition: 0.2s;
            font-weight: 600;
        }
        #hamberger{
            border:5px solid rgb(241, 193, 26);
        }
        #hamberger:hover {
            border: 5px solid #f1f1f1;
        }

        * {
            box-sizing: border-box;
        }

        #myInput {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 80%;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            margin:10px;
            margin-right: 0;
        }

        #mySelect{
            width: 17%;
            font-size: 16px;
            padding: 12px 0 12px 0;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            margin:10px;
            margin-left: 0;
            color: gray;
        }

        #myTable {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #ddd;
            font-size: 18px;
            /*background-color:white;*/
            margin-bottom:40px;
        }

        #myTable th, #myTable td {
            text-align: left;
            padding: 12px;
        }

        #myTable tr {
            border-bottom: 1px solid #ddd;
        }

        #myTable tr.header, #myTable tr:hover {
            background-color: #f1f1f1;
        }

        .myInput {
            background-color: #f1f1f1;
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 100%;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
        }
        #change_password_submit{
            font-size:20px;
            font-weight:400;
            padding: 9px 20px 9px 40px;
            background-color:#ddd;
        }

        #btn-edit{
            font-size: 18px;
            padding: 10px;
            color: #f1c11a;
            background-color: transparent;
            border: 2px solid #f1c11a;
            border-right: 0;
            border-radius: 4px 0 0 4px;
            font-weight: bold;
            transition-duration: .2s;
            text-decoration: none;
        }
        #btn-edit:hover{
            color:white;
            background-color: #f1c11a;
            transition-duration: .2s;
        }
        #btn-delete{
            font-size: 18px;
            padding: 10px;
            color: maroon;
            background-color: transparent;
            border: 2px solid maroon;
            border-left: 0;
            border-radius: 0 4px 4px 0;
            font-weight: bold;
            transition-duration: .2s;
            text-decoration: none;
        }
        #btn-delete:hover{
            color: white;
            background-color: maroon;
            transition-duration: .2s;
        }

        #sel1{
            background-color: #f1f1f1;
            font-size: 14px;
            height:49px;
            border-radius:0;

        }
        .sel1{
            background-color: #f1f1f1;
            font-size: 14px;
            height:49px;
            border-radius:0;

        }
        #sel2{
            background-color: #f1f1f1;
            font-size: 14px;
            height:49px;
            border-radius:0;

        }
        #addflightsubmit{
            font-size:20px;
            font-weight:400;
            padding:7px 20px 7px 20px;
            background-color:#ddd;
            width:100%;
            margin-top:30px;
        }

        #editflightsubmit{
            font-size:20px;
            font-weight:400;
            padding:7px 20px 7px 20px;
            background-color:#ddd;
            width:100%;
            margin-top:30px;
        }

    </style>
</head>
<body>
<div class="pure-container" data-effect="pure-effect-scaleDown">
    <input type="checkbox" id="pure-toggle-right" class="pure-toggle" data-toggle="right" />
    <label class="pure-toggle-label" for="pure-toggle-right" data-toggle-label="right" id="hamberger" style="<?php if(isset($_GET['page']) && ($_GET['page'] == "addflight" or $_GET['page'] == "editflight")){echo 'display: none;';}else{echo 'display: block;';} ?>"><span class="pure-toggle-icon" id="hamberger_span"></span></label>
    <nav class="pure-drawer" data-position="right" style="background-color: #f1f1f1">
        <div class="row collapse" style="display: inline-block;">
            <div class="large-12 columns">
                <ul class="nav-primary" id="myMenu">
                    <!--<li><a href="http://localhost/FlightPlanner">Home</a></li>
                    <li><a href="http://localhost/FlightPlanner/cpanel?page=bookmarked">Bookmarked Flights</a></li>
                    <li><a href="http://localhost/FlightPlanner/cpanel?page=flights">All Flights</a></li>
                    <li><a href="http://localhost/FlightPlanner/cpanel?page=changepassword">Change Password</a></li>
                    <li><a href="http://localhost/FlightPlanner/response.php?action=logout">Log Out</a></li>-->
                    <?php
                    $user_type_id = $_SESSION['user_type_id'];
                    if($user_type_id == Cpanel::$USER_MENU) {
                        file_put_contents("test.txt","dcs");
                        Cpanel::generateMenu(Cpanel::$USER_MENU);
                    }
                    elseif($user_type_id == Cpanel::$ADMIN_MENU){
                        Cpanel::generateMenu(Cpanel::$ADMIN_MENU);
                    }
                    elseif($user_type_id == Cpanel::$SUPER_ADMIN_MENU){
                        Cpanel::generateMenu(Cpanel::$SUPER_ADMIN_MENU);
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="pure-pusher-container">
        <div class="pure-pusher">
            <!--Content-->
            <div style="background:#111112;font-size:50px;color:#f1f1f1;text-align:center;padding:20px;" >
                <span>DON'T MAKE ME WALK WHEN I CAN  </span>
                <span class="wow fadeInUp" data-wow-delay="1.2s" style="font-size:50px;color:#f1c11a">FLY</span>
            </div>
            <div class="container">
                <svg class="svgcolor-light" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" style="bottom:1px;">
                    <path style="fill: #111112;padding:0px;" d="M0 0 L50 100 L100 0 Z"></path>
                </svg>
            </div>
            <div> <!--style="margin-right: 10px;margin-left: 10px;"-->
                <!-- Content -->
                <?php
                    if(isset($_GET['page'])){
                        if($_GET['page'] == "bookmarked"){
                            Cpanel::generateBookmarkedPage();
                        }
                        elseif($_GET['page'] == "flights"){
                            Cpanel::generateAllFlightsPage();
                        }
                        elseif ($_GET['page'] == "changepassword"){
                            Cpanel::generateChangePasswordPage();
                        }
                        elseif ($_GET['page'] == "mydesk"){
                            Cpanel::generateOnDeskFlightsForAdminPage();
                        }
                        elseif ($_GET['page'] == "addflight"){
                            Cpanel::generateAddFlightPage();
                        }
                        elseif ($_GET['page'] == "editflight" && isset($_GET['flight_plan_id']) && is_numeric($_GET['flight_plan_id'])){
                            $result = FlightPlan::isInOrigin($_GET['flight_plan_id']);
                            Cpanel::generateEditFlightPage($result,$_GET['flight_plan_id']);
                        }
                    }
                    else{
                        if($_SESSION['user_type_id']==Cpanel::$USER_MENU) {
                            if (Bookmark::isBookmarkExist($_SESSION['user_id'])) {
                                Cpanel::generateBookmarkedPage();
                            } else {
                                Cpanel::generateAllFlightsPage();
                            }
                        }
                        elseif($_SESSION['user_type_id']==Cpanel::$ADMIN_MENU){
                            Cpanel::generateOnDeskFlightsForAdminPage();
                        }

                    }

                ?>
                <!-- End Of Content -->
            </div>
                <footer>
                    <div class="container">
                        <div class="row">

                            <svg class="svgcolor-light" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0 L50 100 L100 0 Z"></path>
                            </svg>



                            <div class="col-md-4 col-sm-6 col-md-push-4" style="text-align: center;">
                                <h2>pine team</h2>
                                <div class="wow fadeInUp" data-wow-delay="0.3s">
                                    <p style="visibility: visible;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus lacus nulla, eget varius justo tristique ut. Etiam a tellus magna.</p>
                                    <p class="copyright-text" style="visibility: visible;">
                                        Copyright &copy; 2018 Pine Team <br>
                                        Designed by <a rel="nofollow" href="http://t.me/mehdi.golpayegani" target="_parent">PineTeam</a>
                                    </p>
                                </div>
                            </div>



                            <!--<div class="col-md-4 col-sm-5">
                                <h2>Our Studio</h2>
                                <p class="wow fadeInUp" data-wow-delay="0.6s">
                                    The Pine Team is created and, <br>
                                    borned in Isfahan UT. <br>
                                    group working is our slogan, <br>
                                    "we are the best". <br>
                                </p>
                                <ul class="social-icon" >
                                    <li><a href="#" class="fa fa-facebook wow bounceIn" data-wow-delay="0.9s"></a></li>
                                    <li><a href="#" class="fa fa-twitter wow bounceIn" data-wow-delay="1.2s"></a></li>
                                    <li><a href="#" class="fa fa-instagram wow bounceIn" data-wow-delay="1.4s"></a></li>
                                    <li><a href="#" class="fa fa-google-plus wow bounceIn" data-wow-delay="1.6s"></a></li>
                                </ul>
                            </div>-->

                        </div>
                    </div>
                </footer>
            <!--End OF Content-->
        </div>
    </div>

    <label class="pure-overlay" for="pure-toggle-right" data-overlay="right"></label>
</div>

    <script>
        $(".error-alert").hide();
        function effect(){
            $(document).ready(function(){
                $(".error-alert").fadeIn(500);
                $(".error-alert").fadeTo(2000, 500, function () {
                    $(".error-alert").slideUp(500);
                });
            });
        }


        function deleterow(row_id,id) {
            document.getElementById(row_id).style.display = "none";
            $.ajax({
                type: 'POST',
                async: false,
                url: 'ajax_deleteflight.php',
                data: { flight_plan_id:id}
                /*success : function(response) {
                    if(response == "true")
                        result = false;
                    else
                        result = true;
                }*/
            });
        }

        function check(session) {
            var old_password=document.forms["changepassword-form"]["oldpassword"].value;
            var new_password=document.forms["changepassword-form"]["newpassword"].value;
            var c_new_password=document.forms["changepassword-form"]["c_newpassword"].value;
            /*var lowerCaseLetters = /[a-z]/g;
            var upperCaseLetters = /[A-Z]/g;
            var numbers = /[0-9]/g;*/
            var lowerCaseLetters = /[a-z]/;
            var upperCaseLetters = /[A-Z]/;
            var numbers = /[0-9]/;
            var error_details = document.getElementById("error-details");
            var result;
            $.ajax({
                type: 'POST',
                async: false,
                url: 'ajax_changepassword.php',
                data: { old_password:old_password, session:session},
                success : function(response) {
                    if(response == "false")
                        result = false;
                    else
                        result = true;
                }
            });
            if(!result){
                effect();
                return false;
            }
            else if(new_password.length < 8){
                error_details.innerText="Your password must be at least 8 character!";
                effect();
                return false;
            }
            else if(!lowerCaseLetters.test(new_password)){
                error_details.innerText="Your password must include lowercase characters!";
                effect();
                return false;
            }
            else if(!upperCaseLetters.test(new_password)){
                error_details.innerText="Your password must include uppercase characters!";
                effect();
                return false;
            }
            else if(!numbers.test(new_password)){
                error_details.innerText="Your password must include numbers!";
                effect();
                return false;
            }
            else if(new_password!=c_new_password){
                error_details.innerText="Your password and confirmation password do not match!!";
                effect();
                return false;
            }
            else{
                return true;
            }
        }

        function lighter(id,flight_plan_id,user_id){
            var a = document.getElementById(id);
            var action = "";
            if(a.style.color!='yellow'){
                a.style.color='yellow';
                action = "add";
            }
            else{
                a.style.color='gray';
                action = "remove";
            }

            $.ajax({
                type: 'POST',
                url: 'ajax_bookmark.php',
                data: { fp_id:flight_plan_id, u_id:user_id, ac:action}
            });
        }


        function myFunction() {
            var input, input2, filter, filter2, table, tr, td, i, j;
            input = document.getElementById("myInput");
            input2 = document.getElementById("mySelect");
            filter = input.value.toUpperCase();
            filter2= input2.value.toUpperCase();
            table = document.getElementById("myTable");
            switch (filter2){
                case "FLIGHT_NUMBER":
                    j = 0;
                    break;
                case "AIRLINE":
                    j = 1;
                    break;
                case "ORIGIN":
                    j = 2;
                    break;
                case "DESTINATION":
                    j = 3;
                    break;
                default:
                    j = 0;
                    break;
            }
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[j];
                if (td) {
                    if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }

    </script>

<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.min.js"></script>

<script src="../js/vegas.min.js"></script>

<script src="../js/wow.min.js"></script>
<script src="../js/smoothscroll.js"></script>
<script src="../js/custom.js"></script>


<?php
if((isset($_GET['page']) && $_GET['page']=="addflight") or (isset($_GET['page']) && $_GET['page']=="editflight")){
?>
<script type="text/javascript"
        src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
</script>

<script type="text/javascript">
    $('#datetimepicker').datetimepicker({
        format: 'dd/MM/yyyy hh:mm:ss'
    });
</script>
<?php
}
?>

</body>
</html>


