<?php
require_once __DIR__.DIRECTORY_SEPARATOR."constants.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/db.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/user.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/flightplan.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/date.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/suggestion.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/information.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/bookmark.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/cpanel.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/airport.php";
require_once __DIR__.DIRECTORY_SEPARATOR."classes/city.php";
require_once __DIR__.DIRECTORY_SEPARATOR."utility.php";
require_once __DIR__.DIRECTORY_SEPARATOR."PHPMailer/src/PHPMailer.php";
require_once __DIR__.DIRECTORY_SEPARATOR."PHPMailer/src/Exception.php";
require_once __DIR__.DIRECTORY_SEPARATOR."PHPMailer/src/SMTP.php";