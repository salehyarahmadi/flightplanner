<?php
if (!isset($_SESSION)) { session_start(); }
?>
<?php require_once __DIR__.DIRECTORY_SEPARATOR."include.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">

    <title>Flight Planner</title>
    <!--
    Pine Team
    -->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- stylesheets css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">

    <link rel="stylesheet" href="css/et-line-font.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <link rel="stylesheet" href="css/vegas.min.css">
    <link rel="stylesheet" href="css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>


</head>
<body>

<!-- preloader section -->
<section class="preloader">
    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</section>

<!-- home section -->
<svg preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgcolor-light">
    <path d="M0 0 L50 100 L100 0 Z"></path>
</svg>
<section id="home">
    <div class="container">
        <div class="row" style="margin-top:-200px;">

            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="home-thumb">
                    <h1 class="wow fadeInUp" data-wow-delay="0.4s">Hello, we are pine team</h1>
                    <h3 class="wow fadeInUp" data-wow-delay="0.6s">We are almost <strong>ready to launch</strong> our <strong>new creative</strong> website!</h3>
                    <?php
                    if(isset($_SESSION['user_id'])) {
                        $text = "Sign Out!";
                        $data_target = "";
                        $data_toggle = "";
                        $href = "http://localhost/FlightPlanner/response.php?action=logout";
                        $a_text = "My Account";
                        $a_href = "http://localhost/FlightPlanner/cpanel";
                    }
                    else {
                        $text = "Sign In!";
                        $data_target = "#modal1";
                        $data_toggle = "modal";
                        $href = "#";
                        $a_text = "Let's begin";
                        $a_href = "http://localhost/FlightPlanner/index.php#about";
                    }
                    ?>
                    <a href="<?php echo $a_href; ?>" class="btn btn-lg btn-default smoothScroll wow fadeInUp hidden-xs" data-wow-delay="0.8s"><?php echo $a_text; ?></a>
                    <a href="<?php echo $href; ?>" data-toggle="<?php echo $data_toggle; ?>" data-target="<?php echo $data_target; ?>" class="btn btn-lg btn-success smoothScroll wow fadeInUp" data-wow-delay="1.0s"><?php echo $text; ?></a>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- about section -->
<section id="about">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-12">
                <img src="images/about-img.png" class="img-responsive wow fadeInUp" alt="About">
            </div>

            <div class="col-md-6 col-sm-12">
                <div class="about-thumb">
                    <div class="section-title">
                        <h1 class="wow fadeIn" data-wow-delay="0.2s">Flight Planner</h1>
                        <h3 class="wow fadeInUp" data-wow-delay="0.4s">we are the best</h3>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.6s">
                        <p>FlightPlanner is the world's largest flight tracking data company and provides over 10,000 aircraft operators and service companies as well as over 12,000,000 passengers with global flight tracking solutions. <br>
                            FlightPlanner leverages data from air traffic control systems and it's admins in over 55 countries.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<!-- feature section -->
<section id="feature">
    <div class="container">
        <div class="row">

            <svg preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgcolor-light">
                <path d="M0 0 L50 100 L100 0 Z"></path>
            </svg>

            <div class="col-md-4 col-sm-6">
                <div class="media wow fadeInUp" data-wow-delay="0.4s">
                    <div class="media-object media-left">
                        <i class="icon icon-laptop"></i>
                    </div>
                    <div class="media-body">
                        <h2 class="media-heading">Responsive</h2>
                        <p>Responsive web design (RWD) is an approach to web design which makes web pages render well on a variety of devices and window or screen sizes.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="media wow fadeInUp" data-wow-delay="0.8s">
                    <div class="media-object media-left">
                        <i class="icon icon-refresh"></i>
                    </div>
                    <div class="media-body">
                        <h2 class="media-heading">Up To Date</h2>
                        <p>Web designers are expected to have an awareness of usability and if their role involves creating mark up then they are also expected to be up to date with web accessibility guidelines.</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-8">
                <div class="media wow fadeInUp" data-wow-delay="1.2s">
                    <div class="media-object media-left">
                        <i class="icon icon-chat"></i>
                    </div>
                    <div class="media-body">
                        <h2 class="media-heading">Support</h2>
                        <p>Graphic design and typography are utilized to support its usability, influencing how the user performs certain interactions and improving the aesthetic appeal of the design.</p>
                    </div>
                </div>
            </div>

            <div class="clearfix text-center col-md-12 col-sm-12">
                <a href="#contact" class="btn btn-default smoothScroll">Talk to us</a>
            </div>

        </div>
    </div>
</section>

<!-- contact section -->
<section id="contact">
    <div class="container">
        <div class="row">

            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="section-title">
                    <h1 class="wow fadeInUp" data-wow-delay="0.3s">Get in touch</h1>
                    <p class="wow fadeInUp" data-wow-delay="0.6s">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet. Dolore magna aliquam erat volutpat.</p>
                </div>
                <div class="contact-form wow fadeInUp" data-wow-delay="1.0s">
                    <form id="contact-form" method="post" action="response.php">
                        <div class="col-md-6 col-sm-6">
                            <input name="sugg-name" type="text" class="form-control" placeholder="Your Name" required>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input name="sugg-email" type="email" class="form-control" placeholder="Your Email" required>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <textarea name="sugg-message" class="form-control" placeholder="Message" rows="6" required></textarea>
                        </div>
                        <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8">
                            <input name="sugg-submit" type="submit" class="form-control submit" id="submit" value="SEND MESSAGE">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

<!-- footer section -->
<footer>
    <div class="container">
        <div class="row">

            <svg class="svgcolor-light" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0 L50 100 L100 0 Z"></path>
            </svg>

            <div class="col-md-4 col-sm-6">
                <h2>pine team</h2>
                <div class="wow fadeInUp" data-wow-delay="0.3s">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus lacus nulla, eget varius justo tristique ut. Etiam a tellus magna.</p>
                    <p class="copyright-text">Copyright &copy; 2018 Pine Team <br>
                        Designed by <a rel="nofollow" href="http://t.me/mehdi.golpayegani" target="_parent">PineTeam</a></p>
                </div>
            </div>

            <div class="col-md-1 col-sm-1"></div>

            <div class="col-md-4 col-sm-5">
                <h2>Our Studio</h2>
                <p class="wow fadeInUp" data-wow-delay="0.6s">
                    The Pine Team is created and, <br>
                    borned in Isfahan UT. <br>
                    group working is our slogan, <br>
                    "we are the best". <br>

                </p>
                <ul class="social-icon">
                    <li><a href="#" class="fa fa-facebook wow bounceIn" data-wow-delay="0.9s"></a></li>
                    <li><a href="#" class="fa fa-twitter wow bounceIn" data-wow-delay="1.2s"></a></li>
                    <li><a href="#" class="fa fa-instagram wow bounceIn" data-wow-delay="1.4s"></a></li>
                    <li><a href="#" class="fa fa-google-plus wow bounceIn" data-wow-delay="1.6s"></a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>

<!-- Sign In modal -->
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Sign In To Your Account!</h2>
            </div>
            <form action="response.php" method="post" name="login-form" onsubmit="return s_check();">
                <input name="s_email" type="email" class="form-control" placeholder="Email Address">
                <input name="s_password" type="password" class="form-control" placeholder="Password" >
                <input name="s_submit" type="submit" class="form-control" value="Sign In" style="margin-bottom:70px;">
            </form>
            <a href="#" data-toggle="modal" data-target="#modal2" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">Sign Up Here!</span></a>
            <p class="thanks">Thank you for your visiting!</p>
            <div class="alert alert-danger error-alert">
                <strong>Error!</strong>
                <span id="s_error-details">Email or Password is incorrect!</span>
            </div>
        </div>
    </div>
</div>
<!-- sign up modal -->
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <div class="modal-header">
                <button type="button" class="close" id="close-button" data-dismiss="modal" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title">Create An Account!</h2>
            </div>
            <form action="response.php" method="post" name="register-form" onsubmit="return r_check()" >
                <input name="r_email" id="r_email" type="email" class="form-control" placeholder="Email Address">
                <input name="r_password" id="r_password" type="password" class="form-control" placeholder="Password">
                <input name="r_confirm_password" id="r_confirm_password" type="password" class="form-control" placeholder="Confirm Password">
                <input name="r_submit" type="submit" class="form-control" value="Sign Up" style="margin-bottom:-5px;">
            </form>
            <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal1" aria-label="Close"><span aria-hidden="true">Sign In Here!</span></a>
            <p class="thanks">Thank you for your visiting!</p>
            <div class="alert alert-danger error-alert">
                <strong>Error!</strong>
                <span id="r_error-details"></span>
            </div>
        </div>
    </div>
</div>


<!-- Back top -->
<a href="#back-top" class="go-top"><i class="fa fa-angle-up"></i></a>

<!-- javscript js -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

<script src="js/vegas.min.js"></script>

<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/custom.js"></script>
<script>
    $(".error-alert").hide();
    function effect(){
        $(document).ready(function(){
            $(".thanks").hide();
            $(".error-alert").fadeIn(500);
            $(".error-alert").fadeTo(2000, 500, function () {
                $(".error-alert").slideUp(500);
                $(".thanks").slideDown(500);
            });
        });
    }

    function s_check() {
        var email=document.forms["login-form"]["s_email"].value;
        var password=document.forms["login-form"]["s_password"].value;
        var result;
        $.ajax({
            type: 'POST',
            async: false,
            url: 'ajax_login.php',
            data: { email:email, password:password},
            success : function(response) {
                if(response == "false"){
                    result = false;
                }else{
                    result = true;
                }
            }
        });
        if(!result){
            effect();
            return false;
        }
        return true;
    }

    function r_check() {
        /*var lowerCaseLetters = /[a-z]/g;
        var upperCaseLetters = /[A-Z]/g;
        var numbers = /[0-9]/g;*/
        var lowerCaseLetters = /[a-z]/;
        var upperCaseLetters = /[A-Z]/;
        var numbers = /[0-9]/;
        var email = document.forms["register-form"]["r_email"].value;
        var password = document.forms["register-form"]["r_password"].value;
        //var password_input = document.getElementById("r_password");
        var confirm_password = document.forms["register-form"]["r_confirm_password"].value;
        var error_details = document.getElementById("r_error-details");
        //alert(password);
        if(!validateEmail(email)){
            error_details.innerText="Please enter a valid email address!";
            effect();
            return false;
        }
        else if(password!=confirm_password){
            error_details.innerText="Your password and confirmation password do not match!!";
            effect();
            return false;
        }
        else if(password.length < 8){
            error_details.innerText="Your password must be at least 8 character!";
            effect();
            return false;
        }
        else if(!lowerCaseLetters.test(password)){
            error_details.innerText="Your password must include lowercase characters!";
            effect();
            return false;
        }
        else if(!upperCaseLetters.test(password)){
            error_details.innerText="Your password must include uppercase characters!";
            effect();
            return false;
        }
        else if(!numbers.test(password)){
            error_details.innerText="Your password must include numbers!";
            effect();
            return false;
        }
        else{
            return true;
        }
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


</script>
</body>
</html>