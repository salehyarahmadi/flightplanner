<?php
if (!isset($_SESSION)) { session_start(); }
require_once __DIR__.DIRECTORY_SEPARATOR."include.php";

if(isset($_POST['r_submit'])){
    $email = $_POST['r_email'];
    $password = $_POST['r_password'];
    $confirm_password = $_POST['r_confirm_password'];
    $result = User::addUserToUsers($email,$password,$confirm_password);
    if($result == 1){
        Information::generate("Welcome to our website","To activate your account, please click on the link that we sent to your email address");
    }
    else{
        $error = "";
        switch ($result){
            case 0:
                $error = "Your registration has not been successfully completed:(";
                break;
            case -1:
                $error = "Your entered email is not true";
                break;
            case -2:
                $error = "This email has been used before!";
                break;
            case -3:
                $error = "Your password and confirmation password do not match!";
                break;
            case -4:
                $error = "Your password must be at least 8 character!";
                break;
            case -5:
                $error = "Your password must include at least a number!";
                break;
            case -6:
                $error = "Your password must include at least a character!";
                break;
            case -7:
                $error = "Confirmation email not sent!";
                break;
        }
        Information::generate("An Error Occured...!!",$error);
    }
}

if(isset($_POST['s_submit'])){
    $email = $_POST['s_email'];
    $password = $_POST['s_password'];
    $user = User::Login($email,$password);
    if($user){
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_type_id'] = $user->user_type_id;
        $_SESSION['user_email'] = $user->email;
        redirect("cpanel/index.php");
    }
    else{
        Information::generate("Login failed!!!","Email or Password is wrong");
    }
}

if(isset($_POST['sugg-submit'])){
    $name = $_POST['sugg-name'];
    $email = $_POST['sugg-email'];
    $message = $_POST['sugg-message'];
    $result = Suggestion::addSuggestion($name,$email,$message);
    Information::generate("Thank You For Your Feedback","We value and appreciate your compliments, suggestions or complaints in order to improve our website");
}

if(isset($_POST['changepassword_submit'])){
    $old_password = $_POST['oldpassword'];
    $new_password = $_POST['newpassword'];
    $user_id = $_SESSION['user_id'];
    $result = User::changePassword($user_id,$old_password,$new_password);
    if($result == 1){
        redirect("cpanel");
    }
    else{
        redirect("cpanel/index.php?page=changepassword");
    }
}

if(isset($_POST['add-submit'])){
    $flight_number = $_POST['flight_number'];
    $airport = $_POST['airport'];
    $duration = $_POST['duration'];
    $duration *= 3600;
    $takeoff_time = $_POST['takeoff_time'];
    $temp = $takeoff_time[0];
    $takeoff_time[0] = $takeoff_time[3];
    $takeoff_time[3] = $temp;
    $temp = $takeoff_time[1];
    $takeoff_time[1] = $takeoff_time[4];
    $takeoff_time[4] = $temp;
    $timestamp_takeoff_time = strtotime($takeoff_time);
    if(FlightPlan::addFlightPlan($flight_number,$airport,$duration,$timestamp_takeoff_time)){
        redirect("cpanel");
    }
    else{
        redirect("cpanel/index.php?page=addflight");
    }

}

if(isset($_POST['edit-submit'])){
    if(isset($_POST['takeoff_delay'])){
        $takeoff_time = $_POST['edit_takeoff_time'];
        $temp = $takeoff_time[0];
        $takeoff_time[0] = $takeoff_time[3];
        $takeoff_time[3] = $temp;
        $temp = $takeoff_time[1];
        $takeoff_time[1] = $takeoff_time[4];
        $takeoff_time[4] = $temp;
        $timestamp_takeoff_time = strtotime($takeoff_time);
        $new_flight_number = $_POST['edit_flight_number'];
        $new_airport = $_POST['edit_airport'];
        $takeoff_delay = $_POST['takeoff_delay'];
        $flight_plan_id = $_POST['flight_plan_id'];
        $result = FlightPlan::update($new_flight_number,$new_airport,$timestamp_takeoff_time,$takeoff_delay,true,$flight_plan_id);
        if($result){
            redirect("cpanel");
        }
    }
    elseif (isset($_POST['landing_delay'])){
        /*$takeoff_time = $_POST['edit_takeoff_time'];
        $temp = $takeoff_time[0];
        $takeoff_time[0] = $takeoff_time[3];
        $takeoff_time[3] = $temp;
        $temp = $takeoff_time[1];
        $takeoff_time[1] = $takeoff_time[4];
        $takeoff_time[4] = $temp;
        $timestamp_takeoff_time = strtotime($takeoff_time);*/
        //$new_flight_number = $_POST['edit_flight_number'];
        $new_airport = $_POST['edit_airport'];
        $landing_delay = $_POST['landing_delay'];
        $flight_plan_id = $_POST['flight_plan_id'];
        $result = FlightPlan::update(null,$new_airport,null,$landing_delay,false,$flight_plan_id);
        if($result){
            redirect("cpanel");
        }
    }
}

if(isset($_GET['action'])){
    if($_GET['action']=="activate"){
        $activation_code = $_GET['code'];
        if(User::activeUser($activation_code)){
            Information::generate("Thank You!","Your Account is active now");
        }
        else{
            Information::generate("Nope!","An error has occurred");
        }
    }
    elseif ($_GET['action'] == "logout"){
        User::Logout();
    }
}