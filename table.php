<?php require_once __DIR__.DIRECTORY_SEPARATOR."include.php"; ?>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <!--
    Pine Team
    -->
    <title>table</title>


    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">

    <!-- stylesheets css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.min.css">

    <link rel="stylesheet" href="css/et-line-font.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!--<link rel="stylesheet" href="css/vegas.min.css">-->
    <link rel="stylesheet" href="css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Rajdhani:400,500,700' rel='stylesheet' type='text/css'>



    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        * {
            box-sizing: border-box;
        }

        #myInput {
            background-position: 10px 10px;
            background-repeat: no-repeat;
            width: 80%;
            font-size: 16px;
            padding: 12px 20px 12px 40px;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            margin:10px;
            margin-right: 0;
        }

        #mySelect{
            width: 17%;
            font-size: 16px;
            padding: 12px 0 12px 0;
            border: 1px solid #ddd;
            margin-bottom: 12px;
            margin:10px;
            margin-left: 0;
            color: gray;
        }

        #myTable {
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #ddd;
            font-size: 18px;
            /*background-color:white;*/
            margin-bottom:40px;
        }

        #myTable th, #myTable td {
            text-align: left;
            padding: 12px;
        }

        #myTable tr {
            border-bottom: 1px solid #ddd;
        }

        #myTable tr.header, #myTable tr:hover {
            background-color: #f1f1f1;
        }
    </style>
</head>
<body>
<div style="background:#111112;font-size:50px;color:#f1f1f1;text-align:center;padding:20px;" >
    <span>DON'T MAKE ME WALK WHEN I CAN  </span>
    <span class="wow fadeInUp" data-wow-delay="1.2s" style="font-size:50px;color:#f1c11a">FLY</span>
</div>
<div class="container">
    <svg class="svgcolor-light" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg" style="bottom:1px;">
        <path style="fill: #111112;padding:0px;" d="M0 0 L50 100 L100 0 Z"></path>
    </svg>




</div>
<h1 style="color:black;padding:100px 0px 10px 40px;">Flight table</h1>

<input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for ..." title="Type in an airline name">
<select id="mySelect">
    <option value="flight_number">Flight Number</option>
    <option value="airline">AirLine</option>
    <option value="origin">Origin</option>
    <option value="destination">Destination</option>
</select>
<div style="margin-right: 10px;margin-left: 10px;">
<table id="myTable">
    <tr class="header">
        <th style="width:10%;">Fight Number</th>
        <th style="width:11%;">Airline</th>
        <th style="width:17%;">Origin</th>
        <th style="width:17%;">Destination</th>
        <th style="width:15%;">TakeOff Time</th>
        <th style="width:15%;">Landing Delay</th>
        <th style="width:15%;">Landing Time</th>

    </tr>
    <?php
        $flight_plans = FlightPlan::getAllFlightPlans();
        foreach ($flight_plans as $flight_plan){
            ?>
            <tr>
                <td><?php echo $flight_plan->flight_number; ?></td>
                <td><?php echo $flight_plan->agency_name; ?></td>
                <td><?php echo $flight_plan->source_airport_name; ?></td>
                <td><?php echo $flight_plan->destination_airport_name; ?></td>
                <td><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay); ?></td>
                <td><?php echo ($flight_plan->end_delay / 60)." minutes"; ?></td>
                <td style="font-weight: bold;"><?php echo Date::formatter($flight_plan->start_time + $flight_plan->start_delay + $flight_plan->duration + $flight_plan->end_delay); ?></td>
            </tr>
            <?php
        }
    ?>
</table>
</div>
<footer>
    <div class="container">
        <div class="row">

            <svg class="svgcolor-light" preserveAspectRatio="none" viewBox="0 0 100 102" height="100" width="100%" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 0 L50 100 L100 0 Z"></path>
            </svg>

            <div class="col-md-4 col-sm-6">
                <h2>pine team</h2>
                <div class="wow fadeInUp" data-wow-delay="0.3s">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque luctus lacus nulla, eget varius justo tristique ut. Etiam a tellus magna.</p>
                    <p class="copyright-text">
                        Copyright &copy; 2018 Pine Team <br>
                        Designed by <a rel="nofollow" href="http://t.me/mehdi.golpayegani" target="_parent">PineTeam</a>
                    </p>
                </div>
            </div>

            <div class="col-md-1 col-sm-1"></div>

            <div class="col-md-4 col-sm-5">
                <h2>Our Studio</h2>
                <p class="wow fadeInUp" data-wow-delay="0.6s">
                    Lorem ipsum dolor sit amet, <br>
                    consectetur adipiscing elit. <br>
                    Lorem ipsum dolor sit amet, <br>
                    consectetur adipiscing elit. <br>

                </p>
                <ul class="social-icon">
                    <li><a href="#" class="fa fa-facebook wow bounceIn" data-wow-delay="0.9s"></a></li>
                    <li><a href="#" class="fa fa-twitter wow bounceIn" data-wow-delay="1.2s"></a></li>
                    <li><a href="#" class="fa fa-instagram wow bounceIn" data-wow-delay="1.4s"></a></li>
                    <li><a href="#" class="fa fa-google-plus wow bounceIn" data-wow-delay="1.6s"></a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>

<script>
    function myFunction() {
        var input, input2, filter, filter2, table, tr, td, i, j;
        input = document.getElementById("myInput");
        input2 = document.getElementById("mySelect");
        filter = input.value.toUpperCase();
        filter2= input2.value.toUpperCase();
        table = document.getElementById("myTable");
        switch (filter2){
            case "FLIGHT_NUMBER":
                j = 0;
                break;
            case "AIRLINE":
                j = 1;
                break;
            case "ORIGIN":
                j = 2;
                break;
            case "DESTINATION":
                j = 3;
                break;
            default:
                j = 0;
                break;
        }
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[j];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--<script src="js/vegas.min.js"></script>-->

<script src="js/wow.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/custom.js"></script>


</body>
</html>
