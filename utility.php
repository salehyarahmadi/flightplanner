<?php
date_default_timezone_set('Asia/Tehran');
function convertDate($time){
    /*$weekdays = array("Sat" , "Sun" , "Mon" , "Tue" , "Wed" , "Thu" , "Fri");
    $months = array("Farvardgin" , "Ordibehesht" , "Khordad" , "Tir" , "Mordad" , "Shahrivar" ,
        "Mehr" , "Aban" , "Azar" , "Dey" , "Bahman" , "Esfand" );
    $dayNumber = date("d" , $time);
    $day2 = $dayNumber;
    $monthNumber = date("m" , $time);
    $month2 = $monthNumber;
    $year = date("Y",$time);
    $weekDayNumber = date("w" , $time);
    $hour = date("G" , $time);
    $minute = date("i" , $time);
    $second = date("s" , $time);
    switch ($monthNumber)
    {
        case 1:
            ($dayNumber < 20) ? ($monthNumber=10) : ($monthNumber = 11);
            ($dayNumber < 20) ? ($dayNumber+=10) : ($dayNumber -= 19);
            break;
        case 2:
            ($dayNumber < 19) ? ($monthNumber =11) : ($monthNumber =12);
            ($dayNumber < 19) ? ($dayNumber += 12) : ($dayNumber -= 18);
            break;
        case 3:
            ($dayNumber < 21) ? ($monthNumber = 12) : ($monthNumber = 1);
            ($dayNumber < 21) ? ($dayNumber += 10) : ($dayNumber -= 20);
            break;
        case 4:
            ($dayNumber < 21) ? ($monthNumber = 1) : ($monthNumber = 2);
            ($dayNumber < 21) ? ($dayNumber += 11) : ($dayNumber -= 20);
            break;
        case 5:
        case 6:
            ($dayNumber < 22) ? ($monthNumber -= 3) : ($monthNumber -= 2);
            ($dayNumber < 22) ? ($dayNumber += 10) : ($dayNumber -= 21);
            break;
        case 7:
        case 8:
        case 9:
            ($dayNumber < 23) ? ($monthNumber -= 3) : ($monthNumber -= 2);
            ($dayNumber < 23) ? ($dayNumber += 9) : ($dayNumber -= 22);
            break;
        case 10:
            ($dayNumber < 23) ? ($monthNumber = 7) : ($monthNumber = 8);
            ($dayNumber < 23) ? ($dayNumber += 8) : ($dayNumber -= 22);
            break;
        case 11:
        case 12:
            ($dayNumber < 22) ? ($monthNumber -= 3) : ($monthNumber -= 2);
            ($dayNumber < 22) ? ($dayNumber += 9) : ($dayNumber -= 21);
            break;
    }
    $newDate['day'] = $dayNumber;
    $newDate['month_num'] = $monthNumber;
    $newDate['month_name'] = $months[$monthNumber - 1];
    if((date("m" , $time) < 3) or ((date("m" , $time) == 3) and (date("d" , $time) < 21)))
        $newDate['year'] = $year - 622;
    else
        $newDate['year'] = $year - 621;
    if($weekDayNumber == 6)
        $newDate['weekday_num'] = 0;
    else
        $newDate['weekday_num'] = $weekDayNumber + 1;
    $newDate['weekday_name'] = $weekdays[$newDate['weekday_num']];
    $newDate['hour'] = $hour;
    $newDate['minute'] = $minute;
    $newDate['second'] = $second;
    return $newDate;
    $Date = array();*/
    $Date['month_num'] = date('m', $time);
    $Date['month_name'] = date('F', $time);
    $Date['weekday_name'] = date('D', $time);
    $Date['weekday_num'] = date('N', $time);
    $Date['day'] = date('d', $time);
    $Date['year'] = date('Y', $time);
    $Date['hour'] = date('G',$time);
    $Date['minute'] = date('i',$time);
    $Date['second'] = date('s',$time);
    return $Date;
}


function redirect($url){
     if (!headers_sent()){
         header('Location: '.$url); exit;
     }else{
         echo '<script type="text/javascript">';
         echo 'window.location.href="'.$url.'";';
         echo '</script>';
         echo '<noscript>';
         echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
         echo '</noscript>'; exit;
     }
 }

 function getUrl() {
  $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
  $url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
  $url .= $_SERVER["REQUEST_URI"];
  /*if($pos = strpos($url , "#send-comment")){
    $url = substr($url , 0 , $pos);
  }*/
  return $url;
}


